import {isEmptyPlainObject, isEmptyPlainObjectStrict, isObject, isPlainObject} from "./common";

test('Common: isObject', () => {
  expect(isObject(null)).toBe(true)
  expect(isObject(new String('a'))).toBe(true)
  expect(isObject({})).toBe(true)
})

test('Common: isPlainObject', () => {
  expect(isPlainObject(null)).toBe(false)
  expect(isPlainObject(new String('a'))).toBe(false)
  expect(isPlainObject({})).toBe(true)
  expect(isPlainObject([])).toBe(false)
})

test('Common: isEmptyPlainObject', () => {
  class A {
  }

  expect(isEmptyPlainObject(null)).toBe(false)
  expect(isEmptyPlainObject(new String('a'))).toBe(false)
  expect(isEmptyPlainObject({})).toBe(true)
  expect(isEmptyPlainObject({a: 1})).toBe(false)
  expect(isEmptyPlainObject(Object.create({a: 1}))).toBe(true)
  expect(isEmptyPlainObject(A)).toBe(false)
  expect(isEmptyPlainObject(new Proxy({}, {}))).toBe(true)
})

test('Common: isEmptyPlainObjectStrict', () => {
  expect(isEmptyPlainObjectStrict(Object.create({a: 1}))).toBe(false)
  expect(isEmptyPlainObjectStrict(new Proxy({}, {}))).toBe(true)
})