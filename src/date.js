import {toStr} from "./common";

const iDay = 24 * 3600 * 1000,
  iHour = 3600 * 1000,
  iMinute = 60 * 1000

/**
 *
 * @param time {string|number|Date|Object} hours by default
 * @param time.d {number|string} days
 * @param time.h {number|string} hours
 * @param time.m {number|string} minutes
 * @return {Date|undefined}
 */
export function genExpires(time) {
  if (!time) return
  if (toStr.call(time) === '[object Date]') return time
  const date = new Date(), dateTime = date.getTime()
  // days
  if (time * 1 > -Infinity) date.setTime(dateTime + time * iHour); else {
    const d = time.d * 1 || 0, h = time.h * 1 || 0, m = time.m * 1 || 0
    if (d + h + m) date.setTime(dateTime + d * iDay + h * iHour + m * iMinute); else return null
  }
  return date
}