// Short Links
export const version = '0.0.1'
export const toStr = Object.prototype.toString
export const sUnd = 'undefined'
export const sObject = 'object'
export const ssObject = '[object Object]'
export const ssArray = '[object Array]'
export const ssSet = '[object Set]'
export const ssMap = '[object Map]'
export const ssWeakSet = '[object WeakSet]'
export const ssWeakMap = '[object WeakMap]'
export const ssSymbol = '[object Symbol]'
export const sBreaker = '&#8203;'
export const sConnector = '&shy;'
export const u = undefined
export const w = typeof window !== sUnd ? window : global
export const d = w.document
export const f = d && d.createDocumentFragment()
export const noop = () => u
const typesCanBeMerge = {
  [ssObject]: 1,
  [ssArray]: 2,
  [ssSet]: 3,
  [ssMap]: 4
}
// Defines
// Loggers
const sLogPrefix = '[LP.U]'
export const log = console.log.bind(console, sLogPrefix)
export const warn = console.warn.bind(console, sLogPrefix)
export const error = console.error.bind(console, sLogPrefix)
export const info = console.info.bind(console, sLogPrefix)
// Functions
export const isObject = o => typeof o === sObject
export const isArray = o => Array.isArray(o)
export const isSet = o => toStr.call(o) === ssSet
export const isMap = o => toStr.call(o) === ssMap
export const isWeakSet = o => toStr.call(o) === ssWeakSet
export const isWeakMap = o => toStr.call(o) === ssWeakMap
export const isPlainObject = o => toStr.call(o) === ssObject
export const isEmptyPlainObject = o => isPlainObject(o) && !Object.keys(o).length
export const isEmptyPlainObjectStrict = o => {
  if (!isPlainObject(o)) return false
  for (let k in o) return false
  return !Object.getOwnPropertySymbols(o).length;
}
export const isEmptyArray = o => Array.isArray(o) && o.length === 0
export const isEmptyPlain = o => o === null || o === u || Number.isNaN(o) || o === ''
export const isEmptyObject = (o, t) => isEmptyPlainObjectStrict(o) || (
  (t = toStr.call(o)) &&
  (
    (t === ssArray && o.length === 0) ||
    ([ssSet, ssMap, ssWeakSet, ssWeakMap].includes(t) && o.size === 0)
  )
)
export const isEmpty = o => isEmptyPlain(o) || isEmptyObject(o)
export const getMergeType = o => typeof o === sObject && typesCanBeMerge[toStr.call(o)] || 0
export const jsParser = s => (new Function(`return ${s};`))()
export const toArray = o => {
  try {
    return Array.prototype.slice.call(o)
  } catch (_) {
  }
}