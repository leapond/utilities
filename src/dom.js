import {isArray} from "./common";

/**
 * @param {Element|string|[Element|string]} el
 * @param {Element} [root=document]
 * @return {*|Element|{nodeType}}
 */
export function getElement(el, root) {
  if (!el || el.nodeType) return el
  if (isArray(el)) return el.map(el => getElement(el, root))
  if (typeof el === 'string') return (root || document).querySelector(el)
}