import deepCopy from "./src/deepCopy"
import deepMerge from "./src/deepMerge";

export * from "./src/common"
export * from "./src/ns"
export * from "./src/date"
export * from "./src/observer"
export * from "./src/dom"
export {deepCopy, deepMerge}