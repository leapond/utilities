import commonjs from "@rollup/plugin-commonjs";
import nodeResolve from "@rollup/plugin-node-resolve";
import serve from "rollup-plugin-serve";
import leapondReplace from "leapond-rollup-plugin-replace"
import {terser} from "rollup-plugin-terser";
import babel from "@rollup/plugin-babel";
import deepCopy from "./src/deepCopy";
import polyfill from "rollup-plugin-polyfill-inject"

const isDev = process.env.NODE_ENV !== 'production',
  pkg = require('./package.json'),
  pkgName = pkg.name.split('/').pop(),
  pluginsDefault = [commonjs(), nodeResolve()],
  optionsTerser = {
    "module": false,
    "compress": {
      "ecma": 2015,
      "pure_getters": true
    },
    "format": {
      "comments": false
    },
    "safari10": true
  },
  optionsBabel = {
    "all": {
      exclude: [/\/core-js\//],
      babelHelpers: 'bundled',
      "presets": [
        [
          "@babel/preset-env",
          {
            "targets": {
              "node": "current"
            }
          }
        ]
      ]
    },
    "iife": {
      exclude: [/\/core-js\//],
      babelHelpers: 'bundled',
      "presets": [
        [
          "@babel/preset-env",
          {
            "targets": {
              "browsers": "> 0.5%, ie >= 11"
            },
            "modules": false,
            "useBuiltIns": "usage",
            "corejs": {
              "version": "3"
            }
          }
        ]
      ]
    }
  };

const configs = []

attachConfig({dev: isDev, min: false, babel: false}, 'cjs', 'es', 'iife')

if (!isDev) {
  attachConfig({dev: false, min: false, babel: false}, 'cjs', 'es', 'iife')
  attachConfig({dev: false, min: true, babel: false}, 'cjs', 'es', 'iife')

  attachConfig({dev: false, min: false, babel: true}, 'cjs', 'es', 'iife')
  attachConfig({dev: false, min: true, babel: true}, 'cjs', 'es', 'iife')
}

function attachConfig(o, ...formats) {
  const config = {
    input: 'index.js',
    output: [],
    plugins: []
  }

  formats.forEach(format => {
    const output = {
      file: `dist/${pkgName}-bundle.${format}${o.babel ? '.babel' : ''}${o.dev ? '' : '.prod'}${o.min ? '.min' : ''}.js`,
      format: format,
      sourcemap: o.dev
    }
    if (format === 'cjs') output.exports = 'auto'
    if (format === 'iife') output.name = pkg.short
    if (o.babel) {
      output.strict = false
      const configCopy = deepCopy(config)
      configCopy.output.push(output)
      applyPlugins(configCopy, o, format)
      return configs.push(configCopy)
    }
    config.output.push(output)
  })

  if (!o.babel) configs.push(applyPlugins(config, o))
}

function applyPlugins(config, o, format) {
  if (o.babel) config.plugins.push(nodeResolve({browser: true}), commonjs()); else config.plugins.push(...pluginsDefault)

  if (o.dev) config.plugins.push(serve('dist')); else config.plugins.push(leapondReplace({LEAPOND: true}))
  if (o.babel) config.plugins.push(
    polyfill({
      modules: {Proxy: ['proxy-polyfill']},
      exclude: ['**/proxy-polyfill/**']
    }),
    babel(optionsBabel[format] || optionsBabel.all)
  )
  if (o.min) config.plugins.push(terser(optionsTerser))

  return config
}

export default configs