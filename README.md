# Leapond utilities

A set of JavaScript utilities for on-demand using.

## Install

```shell
# npm
npm i @leapond/utilities
# yarn
yarn add @leapond/utilities
```

## Usage

**Development**

```javascript
// on-demand import
import {log, isEmptyPlainObjectStrict} from "@leappond/utilites/src/common";
import deepMerge from "@leapond/utilities/src/deepMerge";
// it will include whole Leapond Utilities
import {log, isObject, deepMerege} from "@leappond/utilites";
```

**Browser**

```html
<!-- Legacy -->
<script src="./utilities-bundle.iife.babel.prod.min.js"></script>
<script>
    lpU.log('batch merge', lpU.deepMerge.batch(
            [
                {a: 1, c: [1, 2, 3], d: new Map([[1, {x: 1, z: 3}]])},
                null,
                {b: 2},
                3,
                {c: [2, 3, 4]},
                {d: new Map([[1, {x: 10, y: 2}]])}
            ],
            {arrayPolicy: deepMerge.ARRAY_CONCAT_UNIQ}
    ))
</script>
<!-- Module -->
<script type="module">
    import * as lpU from "./utilities-bundle.es.js";

    lpU.log('batch merge', lpU.deepMerge.batch(
            [
                {a: 1, c: [1, 2, 3], d: new Map([[1, {x: 1, z: 3}]])},
                null,
                {b: 2},
                3,
                {c: [2, 3, 4]},
                {d: new Map([[1, {x: 10, y: 2}]])}
            ],
            {arrayPolicy: deepMerge.ARRAY_CONCAT_UNIQ}
    ))
</script>
```

## Document

```javascript
deepCopy(target, depthMax)

/**
 * Merge Options
 *
 * {boolean|number} [clone=-Infinity] - if clone target. <br/>> true for all, false for none, <br/>> number<0 for reuse depth, number>0 for clone depth
 * {boolean} [unEnumerableInclude=false] - if include keys not enumerable(and Symbol keys)
 * {arrayMergePolicies} [arrayPolicy=ARRAY_NORMAL] - array merge policy of build-in
 * {function} [arrayMerge] - custom array merger, (a, b) => result
 * {boolean} [deepMap=true] - if dig in Map items
 */
/**
 * Array Merge Policies
 *
 * ARRAY_NORMAL - write by deepMerge
 * ARRAY_NORMAL_FIXED - existed item will be readonly, but can increase new item
 * ARRAY_CONCAT - concat to target array
 * ARRAY_CONCAT_UNIQ - concat to target array, but skip existed item
 * ARRAY_REPLACE - replace whole array
 * ARRAY_SEAL - fixed length
 * ARRAY_FREEZE - ignore source
 */
deepMerge(target, source, options)
deepMerge.batch(aTargets, options)

// patched @nx-js/observer-util
const o = observable({x: 0})
observe(() => console.log('changed:', o.x), {
  scheduler(ok, op) {
    if (op.value > 100) ok(); else console.warn('o.x must large than 100!')
  }
})
o.x = 99
o.x = 2021
```

Refer to the JSDoc/comments in code.

Exported items:

1. log
1. warn
1. error
1. info
1. **deepCopy**
1. **deepMerge**
1. getMergeType
1. isArray
1. isEmpty
1. isEmptyArray
1. isEmptyObject
1. isEmptyPlain
1. isEmptyPlainObject
1. isEmptyPlainObjectStrict
1. isMap
1. isObject
1. isObservable
1. isPlainObject
1. isSet
1. isWeakMap
1. isWeakSet
1. noop
1. **observable**
1. **observe**
1. raw
1. sObject
1. ssArray
1. ssMap
1. ssObject
1. ssSet
1. ssSymbol
1. ssWeakMap
1. ssWeakSet
1. toStr
1. u
1. unobserve
1. version