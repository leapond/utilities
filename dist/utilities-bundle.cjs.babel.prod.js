Object.defineProperty(exports, '__esModule', { value: true });

// Short Links
const version = '0.0.1';
const toStr = Object.prototype.toString;
const sUnd = 'undefined';
const sObject = 'object';
const ssObject = '[object Object]';
const ssArray = '[object Array]';
const ssSet = '[object Set]';
const ssMap = '[object Map]';
const ssWeakSet = '[object WeakSet]';
const ssWeakMap = '[object WeakMap]';
const ssSymbol = '[object Symbol]';
const sBreaker = '&#8203;';
const sConnector = '&shy;';
const u = undefined;
const w = typeof window !== sUnd ? window : global;
const d = w.document;
const f = d && d.createDocumentFragment();
const noop = () => u;
const typesCanBeMerge = {
  [ssObject]: 1,
  [ssArray]: 2,
  [ssSet]: 3,
  [ssMap]: 4
}; // Defines
// Loggers

const sLogPrefix = '[LP.U]';
const log = console.log.bind(console, sLogPrefix);
const warn = console.warn.bind(console, sLogPrefix);
const error = console.error.bind(console, sLogPrefix);
const info = console.info.bind(console, sLogPrefix); // Functions

const isObject = o => typeof o === sObject;
const isArray = o => Array.isArray(o);
const isSet = o => toStr.call(o) === ssSet;
const isMap = o => toStr.call(o) === ssMap;
const isWeakSet = o => toStr.call(o) === ssWeakSet;
const isWeakMap = o => toStr.call(o) === ssWeakMap;
const isPlainObject = o => toStr.call(o) === ssObject;
const isEmptyPlainObject = o => isPlainObject(o) && !Object.keys(o).length;
const isEmptyPlainObjectStrict = o => {
  if (!isPlainObject(o)) return false;

  for (let k in o) return false;

  if (Object.getOwnPropertySymbols(o).length) return false;
  return true;
};
const isEmptyArray = o => Array.isArray(o) && o.length === 0;
const isEmptyPlain = o => o === null || o === u || Number.isNaN(o) || o === '';
const isEmptyObject = (o, t) => isEmptyPlainObjectStrict(o) || (t = toStr.call(o)) && (t === ssArray && o.length === 0 || [ssSet, ssMap, ssWeakSet, ssWeakMap].includes(t) && o.size === 0);
const isEmpty = o => isEmptyPlain(o) || isEmptyObject(o);
const getMergeType = o => typeof o === sObject && typesCanBeMerge[toStr.call(o)] || 0;
const jsParser = s => new Function(`return ${s};`)();

/**
 * Object, Array, Set, Map supported deep copy.
 * @param target
 * @param {number} [depthMax]
 * @return {Map<any, any>|Set<any>|*[]|*}
 */

function deepCopy(target, depthMax = Infinity) {
  if (!target) return target;
  let args = arguments,
      typeTarget = args[2],
      depthCurrent = args[3] || 0,
      aLoops = args[4],
      dest,
      v;
  if (!(typeTarget > -1)) typeTarget = getMergeType(target);
  if (!typeTarget || depthCurrent >= depthMax) return target;
  depthCurrent++;
  aLoops = aLoops ? [...aLoops, target] : [target];

  switch (typeTarget) {
    case 1:
    case 2:
      dest = typeTarget === 1 ? {} : [];
      Object.keys(target).forEach(k => {
        v = target[k];
        dest[k] = aLoops.includes(v) ? v : deepCopy(v, depthMax, -1, depthCurrent, aLoops);
      });
      return dest;

    case 3:
      dest = new Set();
      target.forEach(v => {
        dest.add(deepCopy(v, depthMax, -1, depthCurrent, aLoops));
      });
      return dest;

    case 4:
      dest = new Map();
      target.forEach((v, k) => {
        dest.set(k, deepCopy(v, depthMax, -1, depthCurrent, aLoops));
      });
      return dest;
  }

  return target;
}

const INNER_MARK = Symbol('');
/**
 * @property ARRAY_NORMAL - write by deepMerge
 * @property ARRAY_NORMAL_FIXED - existed item will be readonly, but can increase new item
 * @property ARRAY_CONCAT - concat to target array
 * @property ARRAY_CONCAT_UNIQ - concat to target array, but skip existed item
 * @property ARRAY_REPLACE - replace whole array
 * @property ARRAY_SEAL - fixed length
 * @property ARRAY_FREEZE - ignore source
 */

const arrayMergePolicies = {
  ARRAY_NORMAL: 1,
  ARRAY_NORMAL_FIXED: 1.1,
  ARRAY_CONCAT: 2,
  ARRAY_CONCAT_UNIQ: 2.1,
  ARRAY_REPLACE: 3,
  ARRAY_SEAL: 4,
  ARRAY_FREEZE: 5
};
const optionsDefault = {
  [INNER_MARK]: 1,
  clone: -Infinity,
  unEnumerableInclude: false,
  arrayPolicy: arrayMergePolicies.ARRAY_NORMAL,
  // (a, b) => a.push(b) && a
  arrayMerge: undefined,
  deepMap: true
};
/**
 * @typedef mergeOptions
 * @property {boolean|number} [clone=-Infinity] - if clone target. <br/>> true for all, false for none, <br/>> number<0 for reuse depth, number>0 for clone depth
 * @property {boolean} [unEnumerableInclude=false] - if include keys not enumerable(and Symbol keys)
 * @property {arrayMergePolicies} [arrayPolicy=ARRAY_NORMAL] - array merge policy of build-in
 * @property {function} [arrayMerge] - custom array merger, (a, b) => result
 * @property {boolean} [deepMap=true] - if dig in Map items
 */

/**
 * @param target
 * @param source
 * @param {mergeOptions} [options]
 * @return {*|{}|[]|[]}
 */

function deepMerge(target, source, options) {
  let isBatch = arguments[4],
      isRoot = !options || !options[INNER_MARK],
      aLoops,
      depthCurrent = 0,
      typeTarget,
      typeSource,
      typeIncoming;
  if (isBatch) (isRoot = true) && ([typeTarget, typeSource, typeIncoming] = isBatch);else {
    typeTarget = getMergeType(target);
    typeSource = getMergeType(source);
  } // clone source while target type is not same with source

  if (!typeTarget) return source;
  if (typeTarget !== typeSource) return isBatch && typeIncoming && !typeSource ? target : source; // init loop circle and depth recorder

  if (isRoot) {
    // init options
    options = parseOptions(options); // create new loops

    aLoops = [source];
    depthCurrent = aLoops.depthCurrent = 1;
  } else {
    // concat loops
    const aLoopsPrev = arguments[3];
    aLoops = [...aLoopsPrev, source]; // detect circled loops

    if (aLoopsPrev.includes(source)) return source; // increase current depth

    depthCurrent = aLoopsPrev.depthCurrent + 1;
  } // clone source while target is empty


  if (!target) return source; // store current depth on loops array property

  aLoops.depthCurrent = depthCurrent; // clone target current level

  if (options.clone > 0 && depthCurrent <= options.clone || options.clone < 0 && depthCurrent > -options.clone) target = deepCopy(target, 1);

  switch (typeSource) {
    case 1:
      // Object
      let keys;
      if (options.unEnumerableInclude) keys = [...Object.getOwnPropertyNames(source), ...Object.getOwnPropertySymbols(source)];else keys = Object.keys(source);
      keys.forEach(key => {
        target[key] = deepMerge(target[key], source[key], options, aLoops);
      });
      break;

    case 2:
      // Array
      if (options.arrayMerge) return options.arrayMerge(target, source);

      switch (options.arrayPolicy) {
        case arrayMergePolicies.ARRAY_FREEZE:
          return target;

        case arrayMergePolicies.ARRAY_SEAL:
        case arrayMergePolicies.ARRAY_NORMAL:
        case arrayMergePolicies.ARRAY_NORMAL_FIXED:
          Object.keys(source).forEach(key => {
            if (key in target) {
              if (options.arrayPolicy === arrayMergePolicies.ARRAY_NORMAL_FIXED) return;
            } else if (options.arrayPolicy === arrayMergePolicies.ARRAY_SEAL) return;

            target[key] = deepMerge(target[key], source[key], options, aLoops);
          });
          break;

        case arrayMergePolicies.ARRAY_REPLACE:
          return source;

        case arrayMergePolicies.ARRAY_CONCAT:
          source.forEach((v, i) => {
            target.push(deepMerge(target[i], v, options, aLoops));
          });
          break;

        case arrayMergePolicies.ARRAY_CONCAT_UNIQ:
          source.forEach((v, i) => {
            !target.includes(v) && target.push(deepMerge(target[i], v, options, aLoops));
          });
      }

      break;

    case 3:
      source.forEach(v => target.add(v));
      break;

    case 4:
      source.forEach((v, k) => target.set(k, options.deepMap ? deepMerge(target.get(k), v, options, aLoops) : v));
      break;
  }

  return target;
}
/**
 *
 * @param {Object[]} aTargets
 * @param {mergeOptions} [options]
 * @return {*}
 */


deepMerge.batch = function (aTargets, options) {
  let target, aTypes;
  options = parseOptions(options);
  target = aTargets.shift();
  aTypes = aTargets.map(getMergeType);
  aTargets.forEach((v, index) => target = deepMerge(target, v, options, null, [getMergeType(target), aTypes[index], aTypes.find((t, i) => i > index && t)]));
  return target;
};

function parseOptions(options) {
  options = Object.assign({}, optionsDefault, options);
  if (options.clone === true) options.clone = Infinity;else if (!(options.clone >= -Infinity)) options.clone = optionsDefault.clone;
  return options;
}

Object.assign(deepMerge, arrayMergePolicies);

const iDay = 24 * 3600 * 1000,
      iHour = 3600 * 1000,
      iMinute = 60 * 1000;
/**
 *
 * @param time {string|number|Date|Object} hours by default
 * @param time.d {number|string} days
 * @param time.h {number|string} hours
 * @param time.m {number|string} minutes
 * @return {Date|undefined}
 */

function genExpires(time) {
  if (!time) return;
  if (toStr.call(time) === '[object Date]') return time;
  const date = new Date(),
        dateTime = date.getTime(); // days

  if (time * 1 > -Infinity) date.setTime(dateTime + time * iHour);else {
    const d = time.d * 1 || 0,
          h = time.h * 1 || 0,
          m = time.m * 1 || 0;
    if (d + h + m) date.setTime(dateTime + d * iDay + h * iHour + m * iMinute);else return null;
  }
  return date;
}

var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

/*
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

var proxy = function proxyPolyfill() {
  let lastRevokeFn = null;
  let ProxyPolyfill;
  /**
   * @param {*} o
   * @return {boolean} whether this is probably a (non-null) Object
   */

  function isObject(o) {
    return o ? typeof o === 'object' || typeof o === 'function' : false;
  }

  function validateProto(proto) {
    if (proto !== null && !isObject(proto)) {
      throw new TypeError('Object prototype may only be an Object or null: ' + proto);
    }
  }

  const $Object = Object; // Closure assumes that `{__proto__: null} instanceof Object` is always true, hence why we check against a different name.

  const noop = function () {
    return null;
  };

  const getProto = $Object.getPrototypeOf || ([].__proto__ === Array.prototype ? function getPrototypeOf(O) {
    // If O.[[Prototype]] === null, then the __proto__ accessor won't exist,
    // as it's inherited from `Object.prototype`
    const proto = O.__proto__;
    return isObject(proto) ? proto : null;
  } : noop);
  /**
   * @constructor
   * @param {!Object} target
   * @param {{apply, construct, get, set}} handler
   */

  ProxyPolyfill = function (target, handler) {
    const newTarget = this && this instanceof ProxyPolyfill ? this.constructor : undefined;

    if (newTarget === undefined) {
      throw new TypeError("Constructor Proxy requires 'new'");
    }

    if (!isObject(target) || !isObject(handler)) {
      throw new TypeError('Cannot create proxy with a non-object as target or handler');
    } // Construct revoke function, and set lastRevokeFn so that Proxy.revocable can steal it.
    // The caller might get the wrong revoke function if a user replaces or wraps scope.Proxy
    // to call itself, but that seems unlikely especially when using the polyfill.


    let throwRevoked = function () {};

    lastRevokeFn = function () {
      /** @suppress {checkTypes} */
      target = null; // clear ref

      throwRevoked = function (trap) {
        throw new TypeError(`Cannot perform '${trap}' on a proxy that has been revoked`);
      };
    };

    setTimeout(function () {
      lastRevokeFn = null;
    }, 0); // Fail on unsupported traps: Chrome doesn't do this, but ensure that users of the polyfill
    // are a bit more careful. Copy the internal parts of handler to prevent user changes.

    const unsafeHandler = handler;
    handler = {
      'get': null,
      'set': null,
      'apply': null,
      'construct': null
    };

    for (let k in unsafeHandler) {

      handler[k] = unsafeHandler[k];
    }

    if (typeof unsafeHandler === 'function') {
      // Allow handler to be a function (which has an 'apply' method). This matches what is
      // probably a bug in native versions. It treats the apply call as a trap to be configured.
      handler.apply = unsafeHandler.apply.bind(unsafeHandler);
    } // Create default getters/setters. Create different code paths as handler.get/handler.set can't
    // change after creation.


    const getter = handler.get ? function (prop) {
      throwRevoked('get');
      return handler.get(this, prop, proxy);
    } : function (prop) {
      throwRevoked('get');
      return this[prop];
    };
    const setter = handler.set ? function (prop, value) {
      throwRevoked('set');
      handler.set(this, prop, value, proxy); // TODO(samthor): If the calling code is in strict mode, throw TypeError.
      // if (!status) {
      // It's (sometimes) possible to work this out, if this code isn't strict- try to load the
      // callee, and if it's available, that code is non-strict. However, this isn't exhaustive.
      // }
    } : function (prop, value) {
      throwRevoked('set');
      this[prop] = value;
    };
    const propertyMap = {}; // Define proxy as an object that extends target.[[Prototype]],
    // or a Function (if either it's callable, or apply is set).

    const proto = getProto(target); // can return null in old browsers

    let proxy;
    let isMethod = false;
    let isArray = false;

    if (typeof target === 'function') {
      proxy = function ProxyPolyfill() {
        const usingNew = this && this.constructor === proxy;
        const args = Array.prototype.slice.call(arguments);
        throwRevoked(usingNew ? 'construct' : 'apply'); // TODO(samthor): Closure compiler doesn't know about 'construct', attempts to rename it.

        if (usingNew && handler['construct']) {
          return handler['construct'].call(this, target, args);
        } else if (!usingNew && handler.apply) {
          return handler['apply'](target, this, args);
        } // since the target was a function, fallback to calling it directly.


        if (usingNew) {
          // inspired by answers to https://stackoverflow.com/q/1606797
          args.unshift(target); // pass class as first arg to constructor, although irrelevant
          // nb. cast to convince Closure compiler that this is a constructor

          const f = target.bind.apply(target, args);
          return new f();
        }

        return target.apply(this, args);
      };

      isMethod = true;
    }
    /* else if (target instanceof Array) {
    proxy = [];
    isArray = true;
    } */
    else {
      if (proto) {
        // set delegate proxy proto
        proxy = new proto.constructor();
        const propertyNames = $Object.getOwnPropertyNames(proto);
        propertyNames.forEach(function (prop) {
          const real = $Object.getOwnPropertyDescriptor(proto, prop);
          const desc = {
            enumerable: Boolean(real.enumerable),
            get: getter.bind(target, prop),
            set: setter.bind(target, prop)
          };

          try {
            $Object.defineProperty(proxy, prop, desc);
          } catch (e) {
            // just set
            proxy[prop] = target[prop];
          }

          propertyMap[prop] = true;
        });
      } else {
        proxy = {};
      }
    }

    const __getter__ = '___@getter___';
    const __setter__ = '___@setter___';
    $Object.defineProperty(proxy, __getter__, {
      enumerable: false,

      get() {
        return getter;
      }

    });
    propertyMap[__getter__] = true;
    $Object.defineProperty(proxy, __setter__, {
      enumerable: false,

      get() {
        return setter;
      }

    });
    propertyMap[__setter__] = true; // Clone direct properties (i.e., not part of a prototype).

    const propertyNames = $Object.getOwnPropertyNames(target);
    propertyNames.forEach(function (prop) {
      if (propertyMap[prop] || (isMethod || isArray) && prop in proxy) {
        return; // ignore properties already here, e.g. 'bind', 'prototype' etc
      }

      const real = $Object.getOwnPropertyDescriptor(target, prop);
      const desc = {
        // in array cases the prop should be deleteable
        configurable: true,
        enumerable: Boolean(real.enumerable),
        get: getter.bind(target, prop),
        set: setter.bind(target, prop)
      };
      $Object.defineProperty(proxy, prop, desc);
      propertyMap[prop] = true;
    }); // Set the prototype, or clone all prototype methods (always required if a getter is provided).
    // TODO(samthor): We don't allow prototype methods to be set. It's (even more) awkward.
    // An alternative here would be to _just_ clone methods to keep behavior consistent.

    let prototypeOk = true;

    if (isMethod || isArray) {
      // Arrays and methods are special: above, we instantiate boring versions of these then swap
      // our their prototype later. So we only need to use setPrototypeOf in these cases. Some old
      // engines support `Object.getPrototypeOf` but not `Object.setPrototypeOf`.
      const setProto = $Object.setPrototypeOf || ([].__proto__ === Array.prototype ? function setPrototypeOf(O, proto) {
        validateProto(proto);
        O.__proto__ = proto;
        return O;
      } : noop);

      if (!(proto && setProto(proxy, proto))) {
        prototypeOk = false;
      }
    }

    if (handler.get || !prototypeOk) {
      for (let k in target) {
        if (propertyMap[k]) {
          continue;
        }

        $Object.defineProperty(proxy, k, {
          get: getter.bind(target, k)
        });
      }
    } // The Proxy polyfill cannot handle adding new properties. Seal the target and proxy.
    // MYHACK
    // $Object.seal(target);
    // $Object.seal(proxy);


    return proxy; // nb. if isMethod is true, proxy != this
  };

  ProxyPolyfill.revocable = function (target, handler) {
    const p = new ProxyPolyfill(target, handler);
    return {
      'proxy': p,
      'revoke': lastRevokeFn
    };
  };

  return ProxyPolyfill;
};

/*
 * Copyright 2018 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

(function (scope) {
  if (scope['Proxy']) {
    return;
  }

  scope.Proxy = proxy();
  scope.Proxy['revocable'] = scope.Proxy.revocable;
})('undefined' !== typeof process && '[object process]' === {}.toString.call(process) || 'undefined' !== typeof navigator && navigator.product === 'ReactNative' ? commonjsGlobal : self);

var connectionStore = new WeakMap();
var ITERATION_KEY = Symbol('iteration key');

function storeObservable(obj) {
  // this will be used to save (obj.key -> reaction) connections later
  connectionStore.set(obj, new Map());
}

function registerReactionForOperation(reaction, ref) {
  var target = ref.target;
  var key = ref.key;
  var type = ref.type;

  if (type === 'iterate') {
    key = ITERATION_KEY;
  }

  var reactionsForObj = connectionStore.get(target);
  var reactionsForKey = reactionsForObj.get(key);

  if (!reactionsForKey) {
    reactionsForKey = new Set();
    reactionsForObj.set(key, reactionsForKey);
  } // save the fact that the key is used by the reaction during its current run


  if (!reactionsForKey.has(reaction)) {
    reactionsForKey.add(reaction);
    reaction.cleaners.push(reactionsForKey);
  }
}

function getReactionsForOperation(ref) {
  var target = ref.target;
  var key = ref.key;
  var type = ref.type;
  var reactionsForTarget = connectionStore.get(target);
  var reactionsForKey = new Set();

  if (type === 'clear') {
    reactionsForTarget.forEach(function (_, key) {
      addReactionsForKey(reactionsForKey, reactionsForTarget, key);
    });
  } else {
    addReactionsForKey(reactionsForKey, reactionsForTarget, key);
  }

  if (type === 'add' || type === 'delete' || type === 'clear') {
    var iterationKey = Array.isArray(target) ? 'length' : ITERATION_KEY;
    addReactionsForKey(reactionsForKey, reactionsForTarget, iterationKey);
  }

  return reactionsForKey;
}

function addReactionsForKey(reactionsForKey, reactionsForTarget, key) {
  var reactions = reactionsForTarget.get(key);
  reactions && reactions.forEach(reactionsForKey.add, reactionsForKey);
}

function releaseReaction(reaction) {
  if (reaction.cleaners) {
    reaction.cleaners.forEach(releaseReactionKeyConnection, reaction);
  }

  reaction.cleaners = [];
}

function releaseReactionKeyConnection(reactionsForKey) {
  reactionsForKey.delete(this);
} // reactions can call each other and form a call stack


var reactionStack = [];
var isDebugging = false;

function runAsReaction(reaction, fn, context, args) {
  // do not build reactive relations, if the reaction is unobserved
  if (reaction.unobserved) {
    return Reflect.apply(fn, context, args);
  } // only run the reaction if it is not already in the reaction stack
  // TODO: improve this to allow explicitly recursive reactions


  if (reactionStack.indexOf(reaction) === -1) {
    // release the (obj -> key -> reactions) connections
    // and reset the cleaner connections
    releaseReaction(reaction);

    try {
      // set the reaction as the currently running one
      // this is required so that we can create (observable.prop -> reaction) pairs in the get trap
      reactionStack.push(reaction);
      return Reflect.apply(fn, context, args);
    } finally {
      // always remove the currently running flag from the reaction when it stops execution
      reactionStack.pop();
    }
  }
} // register the currently running reaction to be queued again on obj.key mutations


function registerRunningReactionForOperation(operation) {
  // get the current reaction from the top of the stack
  var runningReaction = reactionStack[reactionStack.length - 1];

  if (runningReaction) {
    debugOperation(runningReaction, operation);
    registerReactionForOperation(runningReaction, operation);
  }
}

function queueReactionsForOperation(operation) {
  // iterate and queue every reaction, which is triggered by obj.key mutation
  getReactionsForOperation(operation).forEach(queueReaction, operation);
}

function queueReaction(reaction) {
  debugOperation(reaction, this); // queue the reaction for later execution or run it immediately

  if (typeof reaction.scheduler === 'function') {
    reaction.scheduler(reaction, this);
  } else if (typeof reaction.scheduler === 'object') {
    reaction.scheduler.add(reaction, this);
  } else {
    reaction();
  }
}

function debugOperation(reaction, operation) {
  if (reaction.debugger && !isDebugging) {
    try {
      isDebugging = true;
      reaction.debugger(operation);
    } finally {
      isDebugging = false;
    }
  }
}

function hasRunningReaction() {
  return reactionStack.length > 0;
}

var IS_REACTION = Symbol('is reaction');

function observe(fn, options) {
  if (options === void 0) options = {}; // wrap the passed function in a reaction, if it is not already one

  var reaction = fn[IS_REACTION] ? fn : function reaction() {
    return runAsReaction(reaction, fn, this, arguments);
  }; // save the scheduler and debugger on the reaction

  reaction.scheduler = options.scheduler;
  reaction.debugger = options.debugger; // save the fact that this is a reaction

  reaction[IS_REACTION] = true; // run the reaction once if it is not a lazy one

  if (!options.lazy) {
    reaction();
  }

  return reaction;
}

function unobserve(reaction) {
  // do nothing, if the reaction is already unobserved
  if (!reaction.unobserved) {
    // indicate that the reaction should not be triggered any more
    reaction.unobserved = true; // release (obj -> key -> reaction) connections

    releaseReaction(reaction);
  } // unschedule the reaction, if it is scheduled


  if (typeof reaction.scheduler === 'object') {
    reaction.scheduler.delete(reaction);
  }
}

var proxyToRaw = new WeakMap();
var rawToProxy = new WeakMap();
var hasOwnProperty = Object.prototype.hasOwnProperty;

function findObservable(obj) {
  var observableObj = rawToProxy.get(obj);

  if (hasRunningReaction() && typeof obj === 'object' && obj !== null) {
    if (observableObj) {
      return observableObj;
    }

    return observable(obj);
  }

  return observableObj || obj;
}

function patchIterator(iterator, isEntries) {
  var originalNext = iterator.next;

  iterator.next = function () {
    var ref = originalNext.call(iterator);
    var done = ref.done;
    var value = ref.value;

    if (!done) {
      if (isEntries) {
        value[1] = findObservable(value[1]);
      } else {
        value = findObservable(value);
      }
    }

    return {
      done: done,
      value: value
    };
  };

  return iterator;
}

var instrumentations = {
  has: function has(key) {
    var target = proxyToRaw.get(this);
    var proto = Reflect.getPrototypeOf(this);
    registerRunningReactionForOperation({
      target: target,
      key: key,
      type: 'has'
    });
    return proto.has.apply(target, arguments);
  },
  get: function get(key) {
    var target = proxyToRaw.get(this);
    var proto = Reflect.getPrototypeOf(this);
    registerRunningReactionForOperation({
      target: target,
      key: key,
      type: 'get'
    });
    return findObservable(proto.get.apply(target, arguments));
  },
  add: function add(key) {
    var target = proxyToRaw.get(this);
    var proto = Reflect.getPrototypeOf(this);
    var hadKey = proto.has.call(target, key); // forward the operation before queueing reactions

    var result = proto.add.apply(target, arguments);

    if (!hadKey) {
      queueReactionsForOperation({
        target: target,
        key: key,
        value: key,
        type: 'add'
      });
    }

    return result;
  },
  set: function set(key, value) {
    var target = proxyToRaw.get(this);
    var proto = Reflect.getPrototypeOf(this);
    var hadKey = proto.has.call(target, key);
    var oldValue = proto.get.call(target, key); // forward the operation before queueing reactions

    var result = proto.set.apply(target, arguments);

    if (!hadKey) {
      queueReactionsForOperation({
        target: target,
        key: key,
        value: value,
        type: 'add'
      });
    } else if (value !== oldValue) {
      queueReactionsForOperation({
        target: target,
        key: key,
        value: value,
        oldValue: oldValue,
        type: 'set'
      });
    }

    return result;
  },
  delete: function delete$1(key) {
    var target = proxyToRaw.get(this);
    var proto = Reflect.getPrototypeOf(this);
    var hadKey = proto.has.call(target, key);
    var oldValue = proto.get ? proto.get.call(target, key) : undefined; // forward the operation before queueing reactions

    var result = proto.delete.apply(target, arguments);

    if (hadKey) {
      queueReactionsForOperation({
        target: target,
        key: key,
        oldValue: oldValue,
        type: 'delete'
      });
    }

    return result;
  },
  clear: function clear() {
    var target = proxyToRaw.get(this);
    var proto = Reflect.getPrototypeOf(this);
    var hadItems = target.size !== 0;
    var oldTarget = target instanceof Map ? new Map(target) : new Set(target); // forward the operation before queueing reactions

    var result = proto.clear.apply(target, arguments);

    if (hadItems) {
      queueReactionsForOperation({
        target: target,
        oldTarget: oldTarget,
        type: 'clear'
      });
    }

    return result;
  },
  forEach: function forEach(cb) {
    var args = [],
        len = arguments.length - 1;

    while (len-- > 0) args[len] = arguments[len + 1];

    var target = proxyToRaw.get(this);
    var proto = Reflect.getPrototypeOf(this);
    registerRunningReactionForOperation({
      target: target,
      type: 'iterate'
    }); // swap out the raw values with their observable pairs
    // before passing them to the callback

    var wrappedCb = function (value) {
      var rest = [],
          len = arguments.length - 1;

      while (len-- > 0) rest[len] = arguments[len + 1];

      return cb.apply(void 0, [findObservable(value)].concat(rest));
    };

    return (ref = proto.forEach).call.apply(ref, [target, wrappedCb].concat(args));
    var ref;
  },
  keys: function keys() {
    var target = proxyToRaw.get(this);
    var proto = Reflect.getPrototypeOf(this);
    registerRunningReactionForOperation({
      target: target,
      type: 'iterate'
    });
    return proto.keys.apply(target, arguments);
  },
  values: function values() {
    var target = proxyToRaw.get(this);
    var proto = Reflect.getPrototypeOf(this);
    registerRunningReactionForOperation({
      target: target,
      type: 'iterate'
    });
    var iterator = proto.values.apply(target, arguments);
    return patchIterator(iterator, false);
  },
  entries: function entries() {
    var target = proxyToRaw.get(this);
    var proto = Reflect.getPrototypeOf(this);
    registerRunningReactionForOperation({
      target: target,
      type: 'iterate'
    });
    var iterator = proto.entries.apply(target, arguments);
    return patchIterator(iterator, true);
  },

  get size() {
    var target = proxyToRaw.get(this);
    var proto = Reflect.getPrototypeOf(this);
    registerRunningReactionForOperation({
      target: target,
      type: 'iterate'
    });
    return Reflect.get(proto, 'size', target);
  }

};

instrumentations[Symbol.iterator] = function () {
  var target = proxyToRaw.get(this);
  var proto = Reflect.getPrototypeOf(this);
  registerRunningReactionForOperation({
    target: target,
    type: 'iterate'
  });
  var iterator = proto[Symbol.iterator].apply(target, arguments);
  return patchIterator(iterator, target instanceof Map);
};

var collectionHandlers = {
  get: function get(target, key, receiver) {
    // instrument methods and property accessors to be reactive
    target = hasOwnProperty.call(instrumentations, key) ? instrumentations : target;
    return Reflect.get(target, key, receiver);
  }
}; // eslint-disable-next-line

var globalObj = typeof window === 'object' ? window : Function('return this')(); // built-in object can not be wrapped by Proxies
// their methods expect the object instance as the 'this' instead of the Proxy wrapper
// complex objects are wrapped with a Proxy of instrumented methods
// which switch the proxy to the raw object and to add reactive wiring

var handlers = new Map([[Map, collectionHandlers], [Set, collectionHandlers], [WeakMap, collectionHandlers], [WeakSet, collectionHandlers], [Object, false], [Array, false], [Int8Array, false], [Uint8Array, false], [Uint8ClampedArray, false], [Int16Array, false], [Uint16Array, false], [Int32Array, false], [Uint32Array, false], [Float32Array, false], [Float64Array, false]]);

function shouldInstrument(ref) {
  var constructor = ref.constructor;
  var isBuiltIn = typeof constructor === 'function' && constructor.name in globalObj && globalObj[constructor.name] === constructor;
  return !isBuiltIn || handlers.has(constructor);
}

function getHandlers(obj) {
  return handlers.get(obj.constructor);
}

var hasOwnProperty$1 = Object.prototype.hasOwnProperty;
var wellKnownSymbols = new Set(Object.getOwnPropertyNames(Symbol).map(function (key) {
  return Symbol[key];
}).filter(function (value) {
  return typeof value === 'symbol';
})); // intercept get operations on observables to know which reaction uses their properties

function get(target, key, receiver) {
  var result = Reflect.get(target, key, receiver); // do not register (observable.prop -> reaction) pairs for well known symbols
  // these symbols are frequently retrieved in low level JavaScript under the hood

  if (typeof key === 'symbol' && wellKnownSymbols.has(key)) {
    return result;
  } // register and save (observable.prop -> runningReaction)


  registerRunningReactionForOperation({
    target: target,
    key: key,
    receiver: receiver,
    type: 'get'
  }); // if we are inside a reaction and observable.prop is an object wrap it in an observable too
  // this is needed to intercept property access on that object too (dynamic observable tree)

  var observableResult = rawToProxy.get(result);

  if (hasRunningReaction() && typeof result === 'object' && result !== null) {
    if (observableResult) {
      return observableResult;
    } // do not violate the none-configurable none-writable prop get handler invariant
    // fall back to none reactive mode in this case, instead of letting the Proxy throw a TypeError


    var descriptor = Reflect.getOwnPropertyDescriptor(target, key);

    if (!descriptor || !(descriptor.writable === false && descriptor.configurable === false)) {
      return observable(result);
    }
  } // otherwise return the observable wrapper if it is already created and cached or the raw object


  return observableResult || result;
}

function has(target, key) {
  var result = Reflect.has(target, key); // register and save (observable.prop -> runningReaction)

  registerRunningReactionForOperation({
    target: target,
    key: key,
    type: 'has'
  });
  return result;
}

function ownKeys(target) {
  registerRunningReactionForOperation({
    target: target,
    type: 'iterate'
  });
  return Reflect.ownKeys(target);
} // intercept set operations on observables to know when to trigger reactions


function set(target, key, value, receiver) {
  // make sure to do not pollute the raw object with observables
  if (typeof value === 'object' && value !== null) {
    value = proxyToRaw.get(value) || value;
  } // save if the object had a descriptor for this key


  var hadKey = hasOwnProperty$1.call(target, key); // save if the value changed because of this set operation

  var oldValue = target[key]; // execute the set operation before running any reaction

  var result = Reflect.set(target, key, value, receiver); // do not queue reactions if the target of the operation is not the raw receiver
  // (possible because of prototypal inheritance)

  if (target !== proxyToRaw.get(receiver)) {
    return result;
  } // queue a reaction if it's a new property or its value changed


  if (!hadKey) {
    queueReactionsForOperation({
      target: target,
      key: key,
      value: value,
      receiver: receiver,
      type: 'add'
    });
  } else if (value !== oldValue) {
    queueReactionsForOperation({
      target: target,
      key: key,
      value: value,
      oldValue: oldValue,
      receiver: receiver,
      type: 'set'
    });
  }

  return result;
}

function deleteProperty(target, key) {
  // save if the object had the key
  var hadKey = hasOwnProperty$1.call(target, key);
  var oldValue = target[key]; // execute the delete operation before running any reaction

  var result = Reflect.deleteProperty(target, key); // only queue reactions for delete operations which resulted in an actual change

  if (hadKey) {
    queueReactionsForOperation({
      target: target,
      key: key,
      oldValue: oldValue,
      type: 'delete'
    });
  }

  return result;
}

var baseHandlers = {
  get: get,
  has: has,
  ownKeys: ownKeys,
  set: set,
  deleteProperty: deleteProperty
};

function observable(obj) {
  if (obj === void 0) obj = {}; // if it is already an observable or it should not be wrapped, return it

  if (proxyToRaw.has(obj) || !shouldInstrument(obj)) {
    return obj;
  } // if it already has a cached observable wrapper, return it
  // otherwise create a new observable


  return rawToProxy.get(obj) || createObservable(obj);
}

function createObservable(obj) {
  // if it is a complex built-in object or a normal object, wrap it
  var handlers = getHandlers(obj) || baseHandlers;
  var observable = new Proxy(obj, handlers); // save these to switch between the raw object and the wrapped object with ease later

  rawToProxy.set(obj, observable);
  proxyToRaw.set(observable, obj); // init basic data structures to save and cleanup later (observable.prop -> reaction) connections

  storeObservable(obj);
  return observable;
}

function isObservable(obj) {
  return proxyToRaw.has(obj);
}

function raw(obj) {
  return proxyToRaw.get(obj) || obj;
}

/**
 * @param {Element|string|[Element|string]} el
 * @param {Element} [root=document]
 * @return {*|Element|{nodeType}}
 */

function getElement(el, root) {
  if (!el || el.nodeType) return el;
  if (isArray(el)) return el.map(el => getElement(el, root));
  if (typeof el === 'string') return (root || document).querySelector(el);
}

exports.d = d;
exports.deepCopy = deepCopy;
exports.deepMerge = deepMerge;
exports.error = error;
exports.f = f;
exports.genExpires = genExpires;
exports.getElement = getElement;
exports.getMergeType = getMergeType;
exports.info = info;
exports.isArray = isArray;
exports.isEmpty = isEmpty;
exports.isEmptyArray = isEmptyArray;
exports.isEmptyObject = isEmptyObject;
exports.isEmptyPlain = isEmptyPlain;
exports.isEmptyPlainObject = isEmptyPlainObject;
exports.isEmptyPlainObjectStrict = isEmptyPlainObjectStrict;
exports.isMap = isMap;
exports.isObject = isObject;
exports.isObservable = isObservable;
exports.isPlainObject = isPlainObject;
exports.isSet = isSet;
exports.isWeakMap = isWeakMap;
exports.isWeakSet = isWeakSet;
exports.jsParser = jsParser;
exports.log = log;
exports.noop = noop;
exports.observable = observable;
exports.observe = observe;
exports.raw = raw;
exports.sBreaker = sBreaker;
exports.sConnector = sConnector;
exports.sObject = sObject;
exports.sUnd = sUnd;
exports.ssArray = ssArray;
exports.ssMap = ssMap;
exports.ssObject = ssObject;
exports.ssSet = ssSet;
exports.ssSymbol = ssSymbol;
exports.ssWeakMap = ssWeakMap;
exports.ssWeakSet = ssWeakSet;
exports.toStr = toStr;
exports.u = u;
exports.unobserve = unobserve;
exports.version = version;
exports.w = w;
exports.warn = warn;
